# MD5_Cracker

<h3><b>INSTALL</b></h3>
<p>You can use the install.sh.</p>

<h3><b>Compiler info</b></h3>

<b>Add the linker flag for the library</b>

`-lcrypto`

<h3>usage:</h3>

<b>prog_name [Options]</b>

<b><i>Options:</i></b>

`-P  {inputfilename}` : Input from list of passwords

`-d` : Database mode

`-s {hash}` : Hash of the password you want to crack

`-v` : Verbose mode

<b><i>Example:<i></b>

```./stpcr -P top-10_000.txt -v -s f52412c4ff1dacd2111f4951f3db1260```

<p>This will crack the hash if it's in the given list and will print every hash and the word.</p>

```./stpcr -d -s f52412c4ff1dacd2111f4951f3db1260```

<p>This will use the Database to crack the password (use this if you have an internet connection)</p>
<p>[!] Database is much faster</p>

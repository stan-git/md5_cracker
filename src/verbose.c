#include "verbose.h"
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

bool Verbose = false;

void setVerbose(bool set) {
	Verbose = set;
}

int v_print(const char *format, ...) {
	if(!Verbose)
		return 0;
		
	va_list args;
	va_start(args,format);
	int ret = vprintf(format, args);
	va_end(args);
	
	return ret;
}

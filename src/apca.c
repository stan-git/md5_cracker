#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


#define PORT 80
#define BUFFER_SIZE 256

char * http_call (char url[], char path[],char hash[]);
void e_print(const char msg[]);

char * http_call (char url[],char path[],char hash[]) 
{
	int sockfd;
	char *resp = (char *)malloc(BUFFER_SIZE);
	struct hostent *server;
	struct sockaddr_in server_addr;
	
	char *get = (char *)malloc((strlen(path)+strlen(hash))+15);
	
	strcat(get,"GET ");
	strcat(get,path);
	strcat(get,hash);
	strcat(get,"\r\n");
		
	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		e_print("Failed to create the socket!");
	
	server = gethostbyname(url);

	if (server == NULL)
		e_print("Can't find Host.");
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	memcpy(&server_addr.sin_addr.s_addr,server->h_addr,server->h_length);
	
	if(connect(sockfd, (struct sockaddr *) &server_addr, sizeof(struct sockaddr_in)) == -1)
		e_print("Couldn't connect.");
		
	write(sockfd, get, strlen(get));

	memset(resp,0,BUFFER_SIZE);

	read(sockfd, resp, BUFFER_SIZE-1);

	free(get);
	close(sockfd);
	
	return resp;
}


void e_print(const char msg[])
{
	printf("[!] ERROR : %s\n",msg);
	exit(EXIT_FAILURE);
}

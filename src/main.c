#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include "verbose.h"
#include <stdbool.h>
#include <unistd.h>
#include "apca.h"

char * crack_by_file(char filename[],char hash[]);
int compare_MD5(char h[],const char s[]);
void print_help(char prog_name[]);


int main(int argc,char *argv[])
{
	char hash[50] = {0}, filename[50] = {0}, *pass;
	int opt, mode = 2;
	
	if(argc < 4)
	{
		print_help(argv[0]);
		exit(EXIT_FAILURE);
	}
	
	while ((opt = getopt(argc, argv, "P:ds:v")) != -1)
	{
		switch (opt)
		{
			case 'P':
					if(strlen(optarg) > 50)
						e_print("File path too long\n");
						
					strcpy(filename,optarg);
					mode = 1;
					
					break;
			
			case 'd':
					mode = 2;
					break;
					
			case 's':
					if(strlen(optarg) > 50)
						abort();
					
					strcpy(hash,optarg);
					break;
			
			case 'v':
					setVerbose(true);
					break;
			default:
				print_help(argv[0]);
				abort();
		}
	}
	
	if(strlen(hash) > 0 && (strlen(filename) || mode == 2)> 0 && mode != 0)
	{
		if (mode == 1)
		{
			pass = crack_by_file(filename,hash);	
			
		} else if (mode == 2 )
		{
			pass = http_call("www.nitrxgen.net",
							"/md5db/",
							hash);
		}
		
	} else {
		print_help(argv[0]);
		return 0;
	}
	
	(pass != NULL && strlen(pass) > 0) ? 
	printf("[*] Found Password: %s\n",pass) :
	printf("[!] Sorry, but I couldn't find anything.\n");
	
	free(pass);
	
	return 0;
}

void print_help(char prog_name[])
{
	printf("usage: %s [Options] \n\
	-P <inputfilename>: Input from list of passwords\n\
	-d : Database mode\n\
	-s <hash>: Hash of the password you want to crack\n\
	-v: Verbose mode\n",prog_name);
}

char * crack_by_file(char filename[],char hash[])
{
	FILE *fp = NULL;
	char *pass = NULL;
	size_t len = 0;
	ssize_t read = 0;
	int count = 0;

	fp = fopen (filename,"r");
	
	if (fp == NULL)
		e_print("Couldn't open file.");
				
	while((read = getline(&pass, &len, fp)) != -1)
	{
		count+=1;
		
		if (count % 100000 == 0)
			printf("[*] Checked %d hashes\n",count);
		
		if(compare_MD5(hash,pass) == 0)
		{
			return pass;
		}
	}

	fclose(fp);
		
	return '\0';
}

int compare_MD5(char h[],const char s[])
{
	//h is a given hash and s a string of a password list
	
	u_char digest[MD5_DIGEST_LENGTH];
	char md5Hash[33];
	
	MD5_CTX c;
	MD5_Init(&c);
	MD5_Update(&c,s,strlen(s)-1);
	MD5_Final(digest,&c);
	
	int i = 0;
	for(; i <= 15; i++)
	{ 
		sprintf(&md5Hash[i*2],"%02x",(u_int) digest[i]);
	}
	
	v_print("%s \t %s\n",md5Hash,s);	
	if(strcmp(h,md5Hash) == 0)
		return 0;
	
	return -1;
}
